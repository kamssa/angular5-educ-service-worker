
import {Component, OnInit} from '@angular/core';

import {Router} from '@angular/router';
import {Message} from 'primeng/primeng';

@Component({
  selector: 'educ-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  title = 'GESTION ECOLE POUR UNE FORMATION DE QUALITE ET D EXCELLENCE';

  constructor(private router: Router) {

  }

  ngOnInit() {

  }

  onInvite() {
    this.router.navigate(['/invite']);
  }

  onEtudiant() {
    this.router.navigate(['/etudiant']);
  }

  onEnseignant() {
    this.router.navigate(['/enseignant']);
  }

  onAdministrateur() {
    this.router.navigate(['/administration']);
  }


}
