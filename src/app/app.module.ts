import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import { AppComponent } from './app.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {FootbarComponent} from './shared/cadre/footbar/footbar.component';
import {MenubarComponent} from './shared/cadre/menubar/menubar.component';
import {NavbarComponent} from './shared/cadre/navbar/navbar.component';
import {MenuModule, PanelMenuModule, SharedModule} from 'primeng/primeng';


@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    FootbarComponent,
    MenubarComponent,
    NavbarComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    PanelMenuModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
